package com.cdm.upv.practica_07;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.telephony.SmsManager;

public class Main2Activity extends AppCompatActivity {

    EditText message;
    String sms, number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        number = intent.getStringExtra(MainActivity.NUMBER);
    }

    public void enviarMensaje(View view){
        message = (EditText)findViewById(R.id.message);
        if(message.getText().toString().isEmpty()){
            Toast toast = Toast.makeText(this, "Ingresa el mensaje", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }else {
            sms = message.getText().toString();

            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(number, null, sms, null, null);
                Toast.makeText(getApplicationContext(), "SMS Enviado", Toast.LENGTH_LONG).show();
            }

            catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Fallo el envio", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
}
